# frozen_string_literal: true

class Customer < ApplicationRecord
  require 'cpf_cnpj'

  validates :name, :cpf, :birthdate, presence: true
  validate :valid_cpf

  def valid_cpf
    errors.add(:cpf, 'cpf is invalid') unless CPF.valid?(cpf)
  end
end
