# frozen_string_literal: true

module CustomerServices
  class ImportFile
    require 'csv'
    def import_csv(file)
      return false if file.blank?

      failed_import_customers = []
      ::CSV.foreach(file, headers: true).each do |row|
        customer = Customer.new(parse_params(row.to_h))
        failed_import_customers << customer unless customer.save
      end
      failed_import_customers
    end

    def parse_params(csv_params)
      {
        cpf: csv_params['CPF'], name: csv_params['Nome'],
        birthdate: csv_params['Data de Nascimento'].to_datetime
      }
    end
  end
end
