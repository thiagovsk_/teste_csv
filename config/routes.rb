# frozen_string_literal: true

Rails.application.routes.draw do
  resources :customers do
    collection { post :import }
  end
  root to: 'customers#index'
end
