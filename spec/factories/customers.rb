# frozen_string_literal: true

FactoryBot.define do
  factory :customer do
    name { 'MyString' }
    cpf { 'MyString' }
    birthdate { '2019-02-25 18:20:52' }
  end
end
